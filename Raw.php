<?php

use Raw\Config;
use Raw\Context;
use Raw\Logger;

final class Raw {

    /**
     * @internal
     * @var Context
     */
    public static ?Raw\Context $_context = null;

    /**
     * Provides access to application global configuration.
     * 
     * @return Config 
     */
    public static function config(): Raw\Config {
        static $config;
        if (!$config) {
            $config = new Raw\Config();
        }
        return $config;
    }

    /**
     * Context specific services, such as database connections
     * and short lived caching for the request.
     * 
     * @return Context 
     */
    public static function context(): Raw\Context {
        if (self::$_context === null) {
            throw new RuntimeException("No context has been created by a request handler.");
        }
        return self::$_context;
    }

    public static function httpClient(): Raw\HttpClient {
        return new Raw\HttpClient();
    }

    /**
     * Provides a system for logging events to a system log
     * 
     * @return Logger 
     */
    public static function logger(): Raw\Logger {
        static $logger;
        if (!$logger) {
            $logger = new Raw\Logger();
        }
        return $logger;
    }

    public static function router(): Raw\Router {
        static $router;
        if (!$router) {
            $router = new Raw\Router();            
        }
        return $router;
    }

    /**
     * Provides per session services which persists on a per
     * browser session (until the session cookie is invalidated).
     * 
     * @return Raw\Session 
     */
    public static function session(): Raw\Session {

    }

    public static function setContext(?Context $context): ?Context {
        $oldContext = self::$_context;
        self::$_context = $context;
        return $oldContext;
    }
}