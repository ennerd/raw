<?php
require("vendor/autoload.php");

$r = new Raw\Router();
$r->add("/users/<id>/edit", "users-id-edit");
$r->add("<id>", "match-all-var");
$r->add("<id>/frode", "start");
$r->add("/users/<id>", "users-id");
$r->dump();
$r->resolve("/users/123");
