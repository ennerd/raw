<?php
namespace Raw;

interface Cache {
    public function set(string $key, mixed $value, float $ttl);
    public function get(string $key, bool &$found=false): mixed;
    public function has(string $key): bool;
    public function unset(string $key): bool;
}