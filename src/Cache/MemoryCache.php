<?php
namespace Raw\Cache;

use Raw\Cache;

class MemoryCache implements Cache {

    private array $cacheVals = [];
    private array $cacheExpires = [];

    public function set(string $key, mixed $value, float $ttl) {
        // LRU: must unset to ensure the value is added to the end
        unset($this->cacheVals[$key], $this->cacheExpires[$key]);

        $this->cacheVals[$key] = \serialize($value);
        $this->cacheExpires[$key] = \microtime(true) + $ttl;
    }

    public function get(string $key, bool &$found = false): mixed {
        $found = $this->has($key);
        if (!$found) {
            return null;
        }

        // LRU: Removing and adding ensures the values are stored at the end
        $expires = $this->cacheExpires[$key];
        $valueSerialized = $this->cacheVals[$key];
        unset( $this->cacheExpires[$key], $this->cacheVals[$key] );
        $this->cacheExpires[$key] = $expires;
        $this->cacheVals[$key] = $expires;

        return \unserialize($valueSerialized);
    }

    public function has(string $key): bool {
        if (!isset($this->cacheVals[$key])) {
            return false;            
        }
        if ($this->cacheExpires[$key] < \microtime(true)) {
            unset( $this->cacheExpires[$key], $this->cacheVals[$key] );
            return false;
        }
        return true;
    }

    public function unset(string $key): bool {
        $found = $this->has($key);
        unset($this->cacheExpires[$key], $this->cacheVals[$key]);
        return $found;
    }

}