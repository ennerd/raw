<?php
namespace Raw;

final class Config {

    /**
     * The root path for the application. Generally this is the folder where
     * the composer.json file resides.
     * 
     * @var string
     */
    public readonly string $rootPath;
    /**
     * The website root URL.
     * 
     * @var string
     */
    public readonly string $baseUrl;

    public readonly string $databaseDSN;
    public readonly string $databaseHost;
    public readonly int $databasePort;
    public readonly string $databaseName;
    public readonly string $databaseUser;
    public readonly string $databasePassword;
    public readonly string $databaseCharset;

    public readonly ?string $httpProxy;
    public readonly float $httpClientTimeout;
    public readonly string $httpClientUserAgent;

    public function __construct() {
        $reflection = new \ReflectionClass(\Composer\Autoload\ClassLoader::class);
        $this->rootPath = dirname($reflection->getFileName(), 2);

        $this->httpProxy = \getenv('HTTP_PROXY') ?: null;

        if ($baseUrl = \getenv("BASE_URL")) {
            $this->baseUrl = $baseUrl;
        } else {
            $baseUrl = "http://";
            if (!(empty($_SERVER["HTTPS"]) || \strtolower($_SERVER["HTTPS"]) == "off")) {
                $baseUrl = "https://";
            }
            $baseUrl .= $_SERVER["HTTP_HOST"];
            $this->baseUrl = $baseUrl;
        }

        $this->loadDatabaseConfig();
        $this->loadHttpClientConfig();
    }

    private function loadDatabaseConfig() {
        if (\getenv('MYSQL_USER')) {
            $parts = [];
            
            if ($host = \getenv('MYSQL_HOST')) {
                $this->databaseHost = $host;
                $parts[] = 'host=' . $host;
            } else {
                $this->databaseHost = "localhost";
            }

            if ($port = \getenv('MYSQL_PORT')) {
                $this->databasePort = (int) $port;
                $parts[] = 'port=' . $port;
            } else {
                $this->databasePort = 3306;
            }
            
            if ($database = \getenv('MYSQL_DATABASE')) {
                $this->databaseName = $database;
                $parts[] = 'dbname=' . $database;
            }

            if ($charset = \getenv('MYSQL_CHARSET')) {
                $this->databaseCharset = $charset;
                $parts[] = 'charset=' . $charset;
            } else {
                $this->databaseCharset = "utf8mb4";
                $parts[] = 'charset=utf8mb4';
            }

            $this->databaseDSN = 'mysql:' . \implode(";", $parts);
            
            $this->databaseUser = \getenv('MYSQL_USER');
            
            $this->databasePassword = \getenv('MYSQL_PASSWORD');
        }
    }

    private function loadHttpClientConfig() {
        $this->httpClientTimeout = (float) (\getenv('HTTP_CLIENT_TIMEOUT') ?: 10);
        $this->httpClientUserAgent = \getenv('HTTP_CLIENT_USER_AGENT') ?: 'Raw/1.0';
    }

}