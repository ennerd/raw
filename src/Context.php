<?php
namespace Raw;

use DateTime;
use PDO;
use Raw;
use Raw\Cache\MemoryCache;

final class Context {

    public array $get;
    public array $post;
    public array $session;
    public array $cookie;
    public array $files;

    private ?DB $db = null;
    private ?PDO $pdo = null;
    private ?Cache $cache = null;
    private Request $request = null;
    private Response $response = null;

    public function __construct(Request $request, Response $response) {
        $this->request = $request;
        $this->response = $response;
    }

    public function db(): DB {
        if (!$this->db) {
            $this->db = new DB($this->pdo());
        }
        return $this->db;
    }

    public function pdo(): PDO {
        if (!$this->pdo) {
            $this->pdo = new PDO(
                Raw::config()->databaseDSN,
                Raw::config()->databaseUser,
                Raw::config()->databasePassword,
                [
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                    PDO::ATTR_EMULATE_PREPARES => false,
                    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_CLASS,
                ]
            );
            $now = new DateTime();
            $mins = $now->getOffset() / 60;
            $sgn = ($mins < 0 ? -1 : 1);
            $mins = abs($mins);
            $hrs = floor($mins / 60);
            $mins -= $hrs * 60;
            $offset = sprintf('%+d:%02d', $hrs*$sgn, $mins);
            
            $this->pdo->exec("SET time_zone='$offset';");
        }
        return $this->pdo;
    }

    public function cache(): Cache {
        if (!$this->cache) {
            $this->cache = new MemoryCache();
        }
        return $this->cache;
    }

    public function request(): Request {
        return $this->request;
    }

    public function response(): Response {
        return $this->response;
    }

    /**
     * Save the super globals $_GET, $_POST, $_SESSION and $_COOKIE
     * to deactivate this context.
     * 
     * @internal This function is automatically invoked by the framework
     *           to isolate separate requests running in the same process.
     * @return void 
     */
    public function deactivateContext() {
        $this->get = $_GET;
        $this->post = $_POST;
        $this->cookie = $_COOKIE;
        $this->session = $_SESSION;
    }

    /**
     * Set the super globals $_GET, $_POST, $_SESSION and $_COOKIE
     * to activate this context.
     * 
     * @internal This function is automatically invoked by the framework
     *           to isolate separate requests running in the same process.
     * @return void 
     */
    public function activateContext() {
        $_GET = $this->get;
        $_POST = $this->post;
        $_COOKIE = $this->cookie;
        $_SESSION = $this->session;
    }

}