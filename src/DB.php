<?php
namespace Raw;

use PDO;
use stdClass;
use Traversable;

final class DB {
    private PDO $pdo;

    public function __construct(PDO $pdo) {
        $this->pdo = $pdo;
    }

    /**
     * Execute a SELECT query and return a Traversable result set
     */
    public function query(string $sql, mixed ...$vars): Traversable {
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($vars);
        while ($row = $stmt->fetch()) {
            yield $row;
        }
    }

    /**
     * Execute a SELECT query and return the first field from the first row
     */
    public function queryField(string $sql, mixed ...$vars): mixed {
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($vars);
        $result = $stmt->fetchColumn();
        return $result !== false ? $result : null;
    }

    /**
     * Execute a SELECT query and return the first row as an object
     */
    public function queryRow(string $sql, mixed ...$vars): ?stdClass {
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($vars);
        $result = $stmt->fetch(PDO::FETCH_OBJ);
        return $result;
    }

    /**
     * Perform a write database query and return the number of affected rows
     */
    public function exec(string $sql, mixed ...$vars): int {
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($vars);
        return $stmt->rowCount();
    }

    /**
     * Get the ID of the last inserted row
     */
    public function lastInsertId(): ?int {
        return $this->pdo->lastInsertId();
    }
}
