<?php
namespace Raw;

use GuzzleHttp\RequestOptions;
use Raw;

class HttpClient extends \GuzzleHttp\Client {

    public function __construct(array $config = [])
    {
        if (!isset($config[RequestOptions::CONNECT_TIMEOUT])) {
            $config[RequestOptions::CONNECT_TIMEOUT] = Raw::config()->httpClientTimeout;
        }
        if (!isset($config[RequestOptions::HEADERS]["User-Agent"])) {
            $config[RequestOptions::HEADERS]["User-Agent"] = Raw::config()->httpClientUserAgent;
        }
        /**
         * @TODO Add a caching middleware.
         */
        parent::__construct($config);
    }

}