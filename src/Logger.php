<?php
namespace Raw;

use Psr\Log\AbstractLogger;
use Psr\Log\LogLevel;

class Logger extends AbstractLogger
{
    private const LEVELS = [
        LogLevel::EMERGENCY,
        LogLevel::ALERT,
        LogLevel::CRITICAL,
        LogLevel::ERROR,
        LogLevel::WARNING,
        LogLevel::NOTICE,
        LogLevel::INFO,
        LogLevel::DEBUG,
    ];

    /**
     * Logs with an arbitrary level.
     *
     * @param mixed  $level
     * @param string $message
     * @param array  $context
     *
     * @return void
     */
    public function log($level, string|\Stringable $message, array $context = []): void
    {
        if (!in_array($level, self::LEVELS)) {
            throw new \Psr\Log\InvalidArgumentException("Invalid log level.");
        }
        $message = $this->interpolate($message, $context);
        $time = \explode(".", (string) microtime(true));
        $ms = \str_pad(\substr($time[1], 0, 3), 3, "0", \STR_PAD_RIGHT);
        $timeString = \gmdate("Y-m-d H:i:s.", $time[0]) . $ms;
        fwrite(STDERR, $timeString . " " . strtoupper($level) . ': ' . $message . PHP_EOL);
    }

    /**
     * Interpolates context values into the message placeholders.
     *
     * @param string $message
     * @param array  $context
     *
     * @return string
     */
    private function interpolate($message, array $context = [])
    {
        // build a replacement array with braces around the context keys
        $replace = [];
        foreach ($context as $key => $val) {
            // check that the value can be casted to string
            if (!is_array($val) && (!is_object($val) || method_exists($val, '__toString'))) {
                $replace['{' . $key . '}'] = $val;
            }
        }

        // interpolate replacement values into the message and return
        return strtr($message, $replace);
    }
}
