<?php
namespace Raw;

interface Request {

    /**
     * Returns the HTTP request verb (GET, POST, PUT etc)
     * @return string 
     */
    public function method(): string;

    /**
     * Returns the value of a query parameter
     * 
     * @param string $key 
     * @return string|string[]|null 
     */
    public function get(string $key): string|array|null;

    /**
     * Returns the value of a form field passed via form database
     * 
     * @param string $key
     * @return string|string[]|null
     */
    public function post(string $key): string|array|null;

    /**
     * Returns the value of an HTTP request header
     * 
     * @param string $key 
     * @return string|null 
     */
    public function header(string $key): ?string;

    /**
     * Returns an uploaded file object.
     * 
     * @param string $key 
     * @return Raw\UploadedFile 
     */
    public function file(string $key): ?UploadedFile;
}