<?php
namespace Raw\Request;

use Raw\Request;

class NativeRequest implements Request {

    public function get(string $key): string|array|null {
        return $_GET[$key] ?? null;
    }

    public function post(string $key): string|array|null {
        return $_POST[$key] ?? null;
    }

    public function header(string $key): ?string {
        $nativeKey = 'HTTP_' . \str_replace("-", "_", \strtoupper($key));
        return $_SERVER[$nativeKey] ?? null;
    }

}