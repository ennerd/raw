<?php
namespace Raw;

interface Response {

    /**
     * Set the HTTP response code and optionally a custom reason.
     * 
     * @param int $code 
     * @param string|null $reason 
     * @return mixed 
     */
    public function status(int $code, string $reason=null);

    /**
     * Set a HTTP response header
     * 
     * @param string $key 
     * @param string $value 
     * @param bool $replace 
     * @return mixed 
     */
    public function header(string $key, string $value, bool $replace=true);

    /**
     * Set the time that this response is allowed to be cached for.
     * 
     * @param int $seconds 
     * @return mixed 
     */
    public function cacheTTL(int $seconds);

    /**
     * Declare that this response can be cached publicly
     * by the browser and by intermediate caching proxies.
     * This function will only restrict the caching visibility
     * (it has no effect if the response has previously been
     * declared with a stricter visibility)
     * 
     * @param int|null $seconds Optionally reduce the time to live
     */
    public function cachePublic(int $seconds=null);

    /**
     * Declare that this response can be cached privately
     * by the browser. This function will only restrict the
     * caching visibility (it has no effect if the response has
     * previously been declared with a stricter visibility)
     * 
     * @param int|null $seconds Optionally reduce the time to live
     */
    public function cachePrivate(int $seconds=null);

    /**
     * Declare that this response can't be cached.
     * 
     * @param bool $noStore Use no-store instead of no-cache 
     */
    public function cacheOff(bool $noStore=false);

}