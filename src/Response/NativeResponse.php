<?php
namespace Raw\Response;

use Raw\Response;

class NativeResponse implements Response {

    private ?int $cacheTTL = null;
    private ?string $cacheVisibility = null;

    /**
     * Set HTTP status code and the associated reason phrase.
     *
     * @param int $code HTTP status code
     * @param string|null $reason HTTP reason phrase (optional)
     */
    public function status(int $code, ?string $reason = null) {
        if ($reason === null) {
            switch ($code) {
                case 100: $reason = "Continue"; break;
                case 101: $reason = "Switching Protocols"; break;
                case 200: $reason = "OK"; break;
                case 201: $reason = "Created"; break;
                case 202: $reason = "Accepted"; break;
                case 203: $reason = "Non-Authoritative Information"; break;
                case 204: $reason = "No Content"; break;
                case 205: $reason = "Reset Content"; break;
                case 206: $reason = "Partial Content"; break;
                case 300: $reason = "Multiple Choices"; break;
                case 301: $reason = "Moved Permanently"; break;
                case 302: $reason = "Found"; break;
                case 303: $reason = "See Other"; break;
                case 304: $reason = "Not Modified"; break;
                case 305: $reason = "Use Proxy"; break;
                case 307: $reason = "Temporary Redirect"; break;
                case 400: $reason = "Bad Request"; break;
                case 401: $reason = "Unauthorized"; break;
                case 402: $reason = "Payment Required"; break;
                case 403: $reason = "Forbidden"; break;
                case 404: $reason = "Not Found"; break;
                case 405: $reason = "Method Not Allowed"; break;
                case 406: $reason = "Not Acceptable"; break;
                case 407: $reason = "Proxy Authentication Required"; break;
                case 408: $reason = "Request Timeout"; break;
                case 409: $reason = "Conflict"; break;
                case 410: $reason = "Gone"; break;
                case 411: $reason = "Length Required"; break;
                case 412: $reason = "Precondition Failed"; break;
                case 413: $reason = "Payload Too Large"; break;
                case 414: $reason = "URI Too Long"; break;
                case 415: $reason = "Unsupported Media Type"; break;
                case 416: $reason = "Range Not Satisfiable"; break;
                case 417: $reason = "Expectation Failed"; break;
                case 426: $reason = "Upgrade Required"; break;
                case 500: $reason = "Internal Server Error"; break;
                case 501: $reason = "Not Implemented"; break;
                case 502: $reason = "Bad Gateway"; break;
                case 503: $reason = "Service Unavailable"; break;
                case 504: $reason = "Gateway Timeout"; break;
                case 505: $reason = "HTTP Version Not Supported"; break;
                default:  $reason = "Unknown HTTP status code: " . $code;
            }
            
        }
        header("HTTP/1.1 " . $code . " " . $reason, true, $code);
    }

    /**
     * Set a HTTP header.
     *
     * @param string $key Header name
     * @param string $value Header value
     * @param bool $replace Should this header replace a previous similar header? (default: true)
     */
    public function header(string $key, string $value, bool $replace = true) {
        header("$key: $value", $replace);
    }

    /**
     * Set a Cache-Control max-age directive without changing the
     * caching visibility.
     *
     * @param int $seconds Number of seconds to cache
     */
    public function cacheTTL(int $seconds) {
        if ($this->cacheVisibility === null) {
            $this->cacheVisibility = 'public';
            $this->cacheTTL = $seconds;
        } else {
            $this->cacheTTL = \min($seconds, $this->cacheTTL);
        }
        $this->setCacheHeaders();
    }

    /**
     * Set a Cache-Control header with public visibility and optionally
     * reduce the max-age for caching the response.
     *
     * @param int|null $seconds Number of seconds to cache (optional)
     */
    public function cachePublic(?int $seconds = null) {
        if ($seconds !== null) {
            $this->cacheTTL($seconds);
        } else {
            if ($this->cacheVisibility === null) {
                $this->cacheVisibility = 'public';
            }
        }
        $this->setCacheHeaders();
    }

    /**
     * Set a Cache-Control header with private visibility and optionally
     * reduce the max-age for caching the response.
     *
     * @param int|null $seconds Number of seconds to cache (optional)
     */
    public function cachePrivate(?int $seconds = null) {
        if ($seconds !== null) {
            $this->cacheTTL($seconds);
        }
        if ($this->cacheVisibility === 'public') {
            $this->cacheVisibility = 'private';
        }
        $this->setCacheHeaders();
    }

    /**
     * Set a Cache-Control header with no-cache visibility and optionally
     * reduce the max-age for caching the response.
     *
     * @param bool $noStore Use no-store instead of no-cache
     */
    public function cacheOff(bool $noStore = false) {
        $this->cacheTTL = 0;
        if ($noStore) {
            $this->cacheVisibility = 'no-store';
        } else {
            if ($this->cacheVisibility !== 'no-store') {
                $this->cacheVisibility = 'no-cache';
            }
        }
        $this->setCacheHeaders();
    }

    private function setCacheHeaders() {
        $cacheTTL = $this->cacheTTL ?? 86400 * 7;
        $this->header("Cache-Control", $this->cacheVisibility . ', max-age=' . $cacheTTL);
    }

}