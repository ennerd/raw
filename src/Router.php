<?php
namespace Raw;

class Router {

    private const RE = '/<([a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*)>/m';
    private array $routes = [];

    function add(string $pattern, mixed $payload) {
        $parts = \preg_split(self::RE, $pattern, -1, \PREG_SPLIT_DELIM_CAPTURE);
        $count = count($parts);
        $pattern = '/^';
        for ($i = 0; $i < $count; $i++) {
            if ($i % 2 == 0) {
                $pattern .= \preg_quote($parts[$i], '/');
            } else {
                if (!empty($parts[$i+1])) {
                    // more to come
                    $pattern .= '(?<' . $parts[$i] . '>[^' . \preg_quote($parts[$i+1][0], '/') . ']+)';
                } else {
                    $pattern .= '(?<' . $parts[$i] . '>.+$)';
                }
            }
        }
        $pattern .= '/';
        $this->routes[$pattern] = $payload;
    }

    public function dump() {
        print_r($this->routes);
    }

    public function resolve(string $string) {
    }
}