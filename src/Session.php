<?php
namespace Raw;

use ArrayAccess;
use DateTimeInterface;

interface Session extends ArrayAccess {

    /**
     * Create a session instance by inspecting the request
     * and optionally updating the response headers.
     * 
     * @return mixed 
     */
    public static function createFromRequest();

    /**
     * Destroy the session and delete all associated data.
     * 
     * @return mixed 
     */
    public function destroy();

    /**
     * Get the time when the session was created.
     * 
     * @return DateTimeInterface 
     */
    public function getStartTime(): DateTimeInterface;

    /**
     * Return the session ID
     * 
     * @return string 
     */
    public function getSessionId(): string;
}