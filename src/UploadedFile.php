<?php
namespace Raw;

class UploadedFile {

    public readonly string $filename;
    public readonly int $filesize;
    public readonly mixed $resource;

    public function __construct(string $filename, int $filesize, mixed $resource) {
        if (!is_resource($resource) || get_resource_type($resource) !== 'stream') {
            throw new \InvalidArgumentException("Invalid resource. A valid stream resource is expected.");
        }

        if ($filesize < 0) {
            throw new \InvalidArgumentException("Invalid file size '{$filesize}'. File size must be a positive integer.");
        }

        $this->filename = $filename;
        $this->filesize = $filesize;
        $this->resource = $resource;
    }
}
